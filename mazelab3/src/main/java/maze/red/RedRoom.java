package maze.red;

import maze.Room;

import java.awt.Color;

/**
 * @author Tobias Highfill
 */
public class RedRoom extends Room {

    public RedRoom(int num) {
        super(num);
    }

    @Override
    public Color getColor() {
        return Color.RED;
    }
}
