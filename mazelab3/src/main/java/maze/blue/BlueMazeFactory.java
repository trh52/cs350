package maze.blue;

import maze.BasicMazeFactory;
import maze.Door;
import maze.Maze;
import maze.MazeGameAbstractFactory;
import maze.Room;
import maze.Wall;

/**
 * @author Tobias Highfill
 */
public class BlueMazeFactory extends MazeGameAbstractFactory {

    @Override
    public Maze makeMaze() {
        return new Maze();
    }

    @Override
    public Wall makeWall() {
        return new Wall();
    }

    @Override
    public Room makeRoom(int num) {
        return new BlueRoom(num);
    }

    @Override
    public Door makeDoor(Room r1, Room r2) {
        return new Door(r1, r2);
    }
}
