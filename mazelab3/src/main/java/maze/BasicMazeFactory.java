package maze;

/**
 * @author Tobias Highfill
 */
public class BasicMazeFactory extends MazeGameAbstractFactory {
    @Override
    public Maze makeMaze() {
        return new Maze();
    }

    @Override
    public Wall makeWall() {
        return new Wall();
    }

    @Override
    public Room makeRoom(int num) {
        return new Room(num);
    }

    @Override
    public Door makeDoor(Room r1, Room r2) {
        return new Door(r1, r2);
    }
}
