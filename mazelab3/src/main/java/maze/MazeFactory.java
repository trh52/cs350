package maze;

/**
 * @author Tobias Highfill
 */
public interface MazeFactory {
    Maze makeMaze();
    Wall makeWall();
    Room makeRoom(int num);
    Door makeDoor(Room r1, Room r2);
}
