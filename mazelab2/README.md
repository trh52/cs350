
CS 350 Lab 3
======

This is the **Factory Method** version.

To run with no command line argument:

```
    make run
```

To run with a file argument passed in:

```
    make runArg
```

This will load the large example maze.

Add `Red` or `Blue` to the end of those target names to change the color.
For example:

```
    make runArgRed
    make runBlue
```