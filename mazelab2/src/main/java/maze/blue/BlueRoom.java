package maze.blue;

import maze.Room;

import java.awt.Color;

/**
 * @author Tobias Highfill
 */
public class BlueRoom extends Room {

    public BlueRoom(int num) {
        super(num);
    }

    @Override
    public Color getColor() {
        return Color.BLUE;
    }
}
