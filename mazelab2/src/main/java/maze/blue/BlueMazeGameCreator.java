package maze.blue;

import maze.MazeGameCreator;
import maze.Room;

/**
 * @author Tobias Highfill
 */
public class BlueMazeGameCreator extends MazeGameCreator {

    @Override
    public Room makeRoom(int num) {
        return new BlueRoom(num);
    }
}
