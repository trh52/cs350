package maze.red;

import maze.Maze;
import maze.MazeGameCreator;
import maze.Room;

/**
 * @author Tobias Highfill
 */
public class RedMazeGameCreator extends MazeGameCreator {

    @Override
    public Room makeRoom(int num) {
        return new RedRoom(num);
    }
}
