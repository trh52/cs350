
CS 350 Lab 2, 3
======

To run with no command line argument:

```
    make run
```

To run with an argument passed in:

```
    make runArg
```

This will load the large example maze.