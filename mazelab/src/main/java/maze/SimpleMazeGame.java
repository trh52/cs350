/*
 * SimpleMazeGame.java
 * Copyright (c) 2008, Drexel University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Drexel University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DREXEL UNIVERSITY ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DREXEL UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package maze;

import maze.ui.MazeViewer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author Sunny
 * @version 1.0
 * @since 1.0
 */
public class SimpleMazeGame
{
	/**
	 * Creates a small maze.
	 */
	public static Maze createMaze()
	{
		Maze maze = new Maze();
		Room r1 = walledRoom(0), r2 = walledRoom(1);
		r1.setSide(Direction.East, r2);
		r2.setSide(Direction.West, r1);
		maze.addRoom(r1);
		maze.addRoom(r2);
		maze.setCurrentRoom(r1);
		return maze;
	}

	private static Room walledRoom(int num){
		Room result = new Room(num);
		for(Direction dir : Direction.values()){
			result.setSide(dir, new Wall());
		}
		return result;
	}

	public static Maze loadMaze(final String path)
	{
		Maze maze = new Maze();
		Map<Integer, String[]> roomSpecs = new HashMap<>(), doorSpecs = new HashMap<>();
		try(FileReader reader = new FileReader(new File(path));
				BufferedReader br = new BufferedReader(reader)){
			for(String line = br.readLine(); line!=null; line = br.readLine()){
				String[] tokens = line.split("\\s+");
				switch(tokens[0]){
					case "room":
						int num = Integer.parseInt(tokens[1]);
						roomSpecs.put(num, tokens);
						maze.addRoom(new Room(num));
						break;
					case "door": doorSpecs.put(Integer.parseInt(tokens[1].substring(1)), tokens); break;
				}
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		Map<Integer, Door> doors = new HashMap<>();
		for(Integer i : doorSpecs.keySet()){
			String[] tokens = doorSpecs.get(i);
			Door door = new Door(maze.getRoom(Integer.parseInt(tokens[2])),
					maze.getRoom(Integer.parseInt(tokens[3])));
			switch(tokens[4]){
				case "open": door.setOpen(true); break;
				case "close": door.setOpen(false); break;
			}
			doors.put(i, door);
		}
		for (Integer rmNum : roomSpecs.keySet()){
			String[] tokens = roomSpecs.get(rmNum);
			Room room = maze.getRoom(rmNum);
			for(Direction dir : Direction.values()){
				String tok = tokens[dir.ordinal()+2];
				if(tok.equals("wall")){
					room.setSide(dir, new Wall());
				}else if(tok.startsWith("d")){
					room.setSide(dir, doors.get(Integer.parseInt(tok.substring(1))));
				}else{
					room.setSide(dir, maze.getRoom(Integer.parseInt(tok)));
				}
			}
		}
		return maze;
	}

	public static void main(String[] args)
	{

		Maze maze = args.length > 0 ? loadMaze(args[0]) : createMaze();
	    MazeViewer viewer = new MazeViewer(maze);
	    viewer.run();
	}
}
