Proctor
=======
By Tobias Highfill

To run the program simply enter:

```
    make
```

This will make gradle build and run the program

###How Files are Saved

Files are saved in JSON format with a file extension of `.proctor`.

###Comments

Sorry I haven't commented this code much, I've been a bit short on time.

I've deviated pretty heavily from my initial class diagram because
I didn't fully grasp the scale of this project.

Just a heads up: I wrote this program in Java 8 and I make heavy use of streams and lambdas.