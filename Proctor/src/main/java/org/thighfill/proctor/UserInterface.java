package org.thighfill.proctor;

import java.io.IOException;
import java.util.List;

/**
 * @author Tobias Highfill
 */
public interface UserInterface {
    String getLine(String prompt) throws IOException;

    String getMultiLine(String prompt) throws IOException;

    default <E> E choose(String prompt, List<E> choices){
        return choose(prompt, choices, false);
    }

    default <E> int chooseIndex(String prompt, List<E> choices){
        return chooseIndex(prompt, choices, false);
    }

    default <E> E choose(String prompt, List<E> choices, boolean allowCancel){
        return choices.get(chooseIndex(prompt, choices, allowCancel));
    }

    <E> int chooseIndex(String prompt, List<E> choices, boolean allowCancel);

    boolean getBoolean(String prompt) throws IOException;

    long getLong(String prompt) throws IOException;

    void showError(String error);

    void showMessage(String message);

    default long getLong(String prompt, long min, long maxInc) throws IOException {
        long res;
        boolean failed = false;
        do {
            if (failed) {
                showError("Answer must be in the closed interval [" + min + ',' + maxInc + "]");
            }
            res = getLong(prompt);
            failed = true;
        } while (res < min || res > maxInc);
        return res;
    }

    default int getInt(String prompt, int min, int maxInc) throws IOException {
        return (int) getLong(prompt, min, maxInc);
    }

    default int getInt(String prompt) throws IOException {
        return getInt(prompt, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }
}
