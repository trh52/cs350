package org.thighfill.proctor.question;

import org.json.JSONException;
import org.json.JSONObject;
import org.thighfill.proctor.UserInterface;

/**
 * @author Tobias Highfill
 */
@FunctionalInterface
public interface JSONConstructor<E extends Question> {
    E construct(UserInterface ui, JSONObject object) throws JSONException;
}
