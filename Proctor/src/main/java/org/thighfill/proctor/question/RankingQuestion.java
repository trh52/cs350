package org.thighfill.proctor.question;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.thighfill.proctor.UserInterface;
import org.thighfill.proctor.question.tabulation.AnswerMap;
import org.thighfill.proctor.question.tabulation.Tabulation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Tobias Highfill
 */
public class RankingQuestion extends GradableQuestion {

    private static final String RANK_CHOICES = "rankChoices";

    private final List<String> _rankChoices = new ArrayList<>();

    public RankingQuestion(UserInterface userInterface, QuestionType type, boolean graded) throws IOException {
        super(userInterface, type, graded);
    }

    public RankingQuestion(UserInterface userInterface, JSONObject json) throws JSONException {
        super(userInterface, json);
        if (json.has(RANK_CHOICES)) {
            JSONArray array = json.getJSONArray(RANK_CHOICES);
            for (int i = 0; i < array.length(); i++) {
                _rankChoices.add(array.getString(i));
            }
        }
    }

    @Override
    public void ask() throws IOException {
        setUserAnswer(getRankings(getUserInterface()));
        setAnswered(true);
    }

    private String getRankings(UserInterface ui) {
        List<String> tmp = new ArrayList<>(_rankChoices);
        List<Integer> order = new ArrayList<>(tmp.size());
        int i = 1;
        while (tmp.size() > 0) {
            String choice =
                    tmp.size() == 1 ? tmp.get(0) : ui.choose(getText() + "\nSelect the " + ordinal(i) + " item", tmp);
            order.add(_rankChoices.indexOf(choice));
            tmp.remove(choice);
            i++;
        }
        return '[' + StringUtils.join(order, ',') + ']';
    }

    private static String ordinal(int i) {
        switch (i % 10) {
        case 1:
            return i + "st";
        case 2:
            return i + "nd";
        case 3:
            return i + "rd";
        default:
            return i + "th";
        }
    }

    @Override
    public JSONObject toJSONObject() throws JSONException {
        return super.toJSONObject().put(RANK_CHOICES, _rankChoices);
    }

    @Override
    protected String toStringInner() {
        StringBuilder builder = new StringBuilder();
        int i = 1;
        for (String s : _rankChoices) {
            if (i > 1) {
                builder.append('\n');
            }
            builder.append('\t').append(s);
            i++;
        }
        return builder.toString();
    }

    private void addNewChoices(UserInterface userInterface, boolean graded) throws IOException {
        while (userInterface.getBoolean("Do you wish to add a new choice?")) {
            _rankChoices.add(userInterface.getLine("Enter the new choice"));
        }
        Set<String> answerSet = getCorrectAnswerSet();
        while (graded && userInterface.getBoolean("Do you wish to add a correct ranking?")) {
            answerSet.add(getRankings(userInterface));
        }
    }

    @Override
    protected void buildWith(UserInterface userInterface, boolean graded) throws IOException {
        addNewChoices(userInterface, graded);
    }

    @Override
    public void modifyWith(UserInterface userInterface, boolean graded) throws IOException {
        super.modifyWith(userInterface, graded);
        Question.modifyChoiceList(_rankChoices, userInterface);
        addNewChoices(userInterface, graded);
    }

    @Override
    public Tabulation createTabulation() {
        return new AnswerMap(this);
    }
}
