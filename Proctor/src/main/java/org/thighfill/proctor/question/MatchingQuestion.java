package org.thighfill.proctor.question;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.thighfill.proctor.UserInterface;
import org.thighfill.proctor.question.tabulation.AnswerMap;
import org.thighfill.proctor.question.tabulation.Tabulation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @author Tobias Highfill
 */
public class MatchingQuestion extends GradableQuestion {

    private static final String CHOICE_SETS = "choiceSets";

    private final List<List<String>> _choiceSets = new ArrayList<>();

    public MatchingQuestion(UserInterface userInterface, QuestionType type, boolean graded) throws IOException {
        super(userInterface, type, graded);
    }

    public MatchingQuestion(UserInterface userInterface, JSONObject json) throws JSONException {
        super(userInterface, json);
        if (json.has(CHOICE_SETS)) {
            JSONArray sets = json.getJSONArray(CHOICE_SETS);
            for (int i = 0; i < sets.length(); i++) {
                JSONArray set = sets.getJSONArray(i);
                List<String> newSet = new ArrayList<>(set.length());
                for (int j = 0; j < set.length(); j++) {
                    newSet.add(set.getString(j));
                }
                _choiceSets.add(newSet);
            }
        }
    }

    @Override
    public void ask() throws IOException {
        setUserAnswer(getMatching(getUserInterface()));
        setAnswered(true);
    }

    private String getMatching(UserInterface ui) throws IOException {
        ui.showMessage(getText() + '\n' + toStringInner());
        List<String> groups = new ArrayList<>();
        while (ui.getBoolean("Would you like to add a matching?")) {
            Integer[] ints = new Integer[_choiceSets.size()];
            boolean done = false;
            while (!done) {
                String[] tokens;
                do {
                    tokens = ui.getLine(
                            "Please enter " + ints.length + " integer values separated by non-digit characters")
                            .split("\\D+");
                } while (tokens.length != ints.length);
                done = true;
                for (int i = 0; i < ints.length; i++) {
                    int val = Integer.parseInt(tokens[i]);
                    if (val < 1 || val > _choiceSets.get(i).size()) {
                        done = false;
                        ui.showError("Value " + val + " (for column #" + (i + 1) + ") is out of range");
                        break;
                    }
                    else {
                        ints[i] = val;
                    }
                }
            }
            groups.add(StringUtils.join(ints, ','));
        }
        return StringUtils.join(groups, ';');
    }

    private List<String> newChoices(UserInterface ui) throws IOException {
        List<String> newSet = new ArrayList<>();
        while (ui.getBoolean("Would you like to add a choice?")) {
            newSet.add(ui.getLine("Enter the choice"));
        }
        return newSet;
    }

    @Override
    protected void buildWith(UserInterface ui, boolean graded) throws IOException {
        while (ui.getBoolean("Would you like to add a choice set?")) {
            _choiceSets.add(newChoices(ui));
        }
        Set<String> answers = getCorrectAnswerSet();
        while (graded && ui.getBoolean("Would you like to add a correct answer?")) {
            answers.add(getMatching(ui));
        }
    }

    private void showChoices(UserInterface userInterface){
        userInterface.showMessage("Current choices:\n" + toStringInner());
    }

    private int chooseSet(UserInterface userInterface, String verb){
        showChoices(userInterface);
        return Integer.parseInt(userInterface.choose("Which set do you want to "+verb+'?',
                IntStream.rangeClosed(0, _choiceSets.size()).mapToObj(i->i==0?"Cancel":("Column "+i))
                        .collect(Collectors.toList())));
    }

    @Override
    public void modifyWith(UserInterface userInterface, boolean graded) throws IOException {
        super.modifyWith(userInterface, graded);
        while (!_choiceSets.isEmpty() && userInterface.getBoolean("Would you like to remove a choice set?")) {
            int choice = chooseSet(userInterface, "remove");
            if(choice > 0)
                _choiceSets.remove(choice-1);
        }
        if(!_choiceSets.isEmpty()){
            showChoices(userInterface);
            while(userInterface.getBoolean("Would you like to modify a choice set?")){
                int setNum = chooseSet(userInterface, "modify");
                if(setNum > 0){
                    List<String> set = _choiceSets.get(setNum-1);
                    Question.modifyChoiceList(set, userInterface);
                    set.addAll(newChoices(userInterface));
                }
            }
        }
        buildWith(userInterface, graded);
    }

    @Override
    public Tabulation createTabulation() {
        return new AnswerMap(this);
    }

    @Override
    protected String toStringInner() {
        int maxChoices = _choiceSets.stream().map(List::size).max(Comparator.naturalOrder()).orElse(-1);
        if (maxChoices < 0) {
            return "No choice sets";
        }
        String[][] choiceMatrix = new String[maxChoices][_choiceSets.size()];
        int[] maxColumns = new int[_choiceSets.size()];
        for (int i = 0; i < maxChoices; i++) {
            int j = 0;
            for (List<String> list : _choiceSets) {
                String tmp = choiceMatrix[i][j] = i < list.size() ? ((i + 1) + ") " + list.get(i)) : "";
                maxColumns[j] = Math.max(maxColumns[j], tmp.length());
                j++;
            }
        }
        StringBuilder builder = new StringBuilder();
        int i = 0;
        for (String[] row : choiceMatrix) {
            if (i > 0) {
                builder.append('\n');
            }
            int j = 0;
            for (String entry : row) {
                if (j > 0) {
                    builder.append("| ");
                }
                builder.append(StringUtils.rightPad(entry, maxColumns[j], ' '));
                j++;
            }
            i++;
        }
        return builder.toString();
    }

    @Override
    public JSONObject toJSONObject() throws JSONException {
        return super.toJSONObject()
                .put(CHOICE_SETS, _choiceSets.stream().map(JSONArray::new).collect(Collectors.toList()));
    }


}
