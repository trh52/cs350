package org.thighfill.proctor.question;

/**
 * @author Tobias Highfill
 */
public enum QuestionGrade {
    INCORRECT,
    CORRECT,
    UNGRADEABLE,
    UNANSWERED
}
