package org.thighfill.proctor.question;

import org.thighfill.proctor.UserInterface;

import java.io.IOException;

/**
 * @author Tobias Highfill
 */
@FunctionalInterface
public interface BasicConstructor<E extends Question> {
    E construct(UserInterface ui, QuestionType type, boolean graded) throws IOException;
}
