package org.thighfill.proctor.question;

import org.json.JSONException;
import org.json.JSONObject;
import org.thighfill.proctor.UserInterface;
import org.thighfill.proctor.question.tabulation.AnswerMap;
import org.thighfill.proctor.question.tabulation.Tabulation;

import java.io.IOException;
import java.util.Set;

/**
 * @author Tobias Highfill
 */
public class ShortAnswerQuestion extends GradableQuestion {

    public ShortAnswerQuestion(UserInterface userInterface, QuestionType type, boolean graded) throws IOException {
        super(userInterface, type, graded);
    }

    public ShortAnswerQuestion(UserInterface userInterface, JSONObject json) throws JSONException {
        super(userInterface, json);
    }

    @Override
    protected String toStringInner() {
        return "";
    }

    @Override
    public void ask() throws IOException {
        setUserAnswer(getUserInterface().getLine(getText()));
        setAnswered(true);
    }

    private void updateAnswers(UserInterface userInterface, boolean graded) throws IOException {
        Set<String> answerSet = getCorrectAnswerSet();
        while (graded && userInterface.getBoolean("Do you wish to add a correct answer?")) {
            answerSet.add(userInterface.getLine("Enter the answer"));
        }
        if (graded) {
            setIgnoreCase(userInterface.getBoolean("Do you want to ignore character case?"));
        }
    }

    @Override
    protected void buildWith(UserInterface userInterface, boolean graded) throws IOException {
        updateAnswers(userInterface, graded);
    }

    @Override
    public void modifyWith(UserInterface userInterface, boolean graded) throws IOException {
        super.modifyWith(userInterface, graded);
        updateAnswers(userInterface, graded);
    }

    @Override
    public Tabulation createTabulation() {
        return new AnswerMap(this);
    }
}
