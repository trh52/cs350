package org.thighfill.proctor.question;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.thighfill.proctor.UserInterface;
import org.thighfill.proctor.question.tabulation.AnswerMap;
import org.thighfill.proctor.question.tabulation.Tabulation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Tobias Highfill
 */
public class MultipleChoiceQuestion extends GradableQuestion {

    static final String MC_ENTRIES = "entries";

    private final List<String> _choices = new ArrayList<>();

    public MultipleChoiceQuestion(UserInterface userInterface, QuestionType type, boolean graded) throws IOException {
        super(userInterface, type, graded);
    }

    public MultipleChoiceQuestion(UserInterface userInterface, JSONObject json) throws JSONException {
        super(userInterface, json);
        if (json.has(MC_ENTRIES)) {
            JSONArray entries = json.getJSONArray(MC_ENTRIES);
            for (int i = 0; i < entries.length(); i++) {
                addChoice(entries.getString(i));
            }
        }
    }

    public void addChoice(String text) {
        _choices.add(text);
    }

    public int choiceCount() {
        return _choices.size();
    }

    @Override
    public JSONObject toJSONObject() throws JSONException {
        return super.toJSONObject().put(MC_ENTRIES, _choices);
    }

    @Override
    public void ask() {
        setUserAnswer(getUserInterface().choose(getText(), _choices));
        setAnswered(true);
    }

    private void addNewChoices(UserInterface userInterface, boolean graded) throws IOException {
        Set<String> answerSet = getCorrectAnswerSet();
        while (userInterface.getBoolean("Do you want to add a choice?")) {
            String answer = userInterface.getLine("Enter the choice text");
            addChoice(answer);
            if (graded && userInterface.getBoolean("Is that answer correct?")) {
                answerSet.add(answer);
            }
        }
    }

    @Override
    protected void buildWith(UserInterface userInterface, boolean graded) throws IOException {
        addNewChoices(userInterface, graded);
    }

    @Override
    public void modifyWith(UserInterface ui, boolean graded) throws IOException {
        super.modifyWith(ui, graded);
        if (!areChoicesImmutable()) {
            Question.modifyChoiceList(_choices, ui);
            addNewChoices(ui, graded);
        }
    }

    @Override
    public Tabulation createTabulation() {
        return new AnswerMap(this);
    }

    @Override
    protected String toStringInner() {
        StringBuilder builder = new StringBuilder();
        int i = 1;
        for (String s : _choices) {
            if (i > 1) {
                builder.append('\n');
            }
            builder.append('\t').append(i).append(") ").append(s);
            i++;
        }
        return builder.toString();
    }

    protected boolean areChoicesImmutable() {
        return false;
    }
}
