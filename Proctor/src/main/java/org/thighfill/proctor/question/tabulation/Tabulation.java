package org.thighfill.proctor.question.tabulation;

import org.thighfill.proctor.question.Question;

/**
 * @author Tobias Highfill
 */
public abstract class Tabulation {
    private final Question _source;

    public Tabulation(Question source) {
        _source = source;
    }

    public abstract void add(Question question);

    protected abstract String showResults();

    @Override
    public String toString() {
        return _source.toString() + "\nRESULTS:\n" + showResults();
    }

    public Question getSource() {
        return _source;
    }
}
