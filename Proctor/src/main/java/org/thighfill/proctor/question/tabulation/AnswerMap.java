package org.thighfill.proctor.question.tabulation;

import org.apache.commons.lang.StringUtils;
import org.thighfill.proctor.question.Question;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Tobias Highfill
 */
public class AnswerMap extends Tabulation {

    Map<String, Integer> _answerMap = new HashMap<>();

    public AnswerMap(Question source) {
        super(source);
        add(source);
    }

    @Override
    public void add(Question question) {
        String ans = question.getUserAnswer();
        _answerMap.put(ans, 1 + _answerMap.getOrDefault(ans, 0));
    }

    @Override
    protected String showResults() {
        return StringUtils.join(_answerMap.entrySet().stream().map(ent -> ent.getKey() + ": " + ent.getValue())
                .collect(Collectors.toList()), "\n");
    }
}
