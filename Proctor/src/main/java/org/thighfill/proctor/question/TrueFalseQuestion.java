package org.thighfill.proctor.question;

import org.json.JSONException;
import org.json.JSONObject;
import org.thighfill.proctor.UserInterface;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;

/**
 * @author Tobias Highfill
 */
public class TrueFalseQuestion extends MultipleChoiceQuestion {

    public TrueFalseQuestion(UserInterface userInterface, QuestionType type, boolean graded) throws IOException {
        super(userInterface, type, graded);
    }

    public TrueFalseQuestion(UserInterface userInterface, JSONObject json) throws JSONException {
        super(userInterface, json);
        if (choiceCount() == 0) {
            addChoice("True");
            addChoice("False");
        }
        setIgnoreCase(true);
    }

    private boolean askForTruthiness(UserInterface userInterface) throws IOException {
        return userInterface.getBoolean("Is that statement true?");
    }

    @Override
    protected void buildWith(UserInterface userInterface, boolean graded) throws IOException {
        if (graded) {
            getCorrectAnswerSet().add("" + askForTruthiness(userInterface));
        }
    }

    @Override
    public void modifyWith(UserInterface ui, boolean graded) throws IOException {
        super.modifyWith(ui, graded);
        if(graded){
            Set<String> answerSet = getCorrectAnswerSet();
            answerSet.removeAll(new ArrayList<>(answerSet));
            answerSet.add("" + askForTruthiness(ui));
        }
    }

    @Override
    protected boolean areChoicesImmutable() {
        return true;
    }
}
