package org.thighfill.proctor.question.tabulation;

import org.apache.commons.lang.StringUtils;
import org.thighfill.proctor.question.Question;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Tobias Highfill
 */
public class AnswerList extends Tabulation {

    List<String> _answers = new ArrayList<>();

    public AnswerList(Question source) {
        super(source);
        add(source);
    }

    @Override
    public void add(Question question) {
        _answers.add(question.getUserAnswer());
    }

    @Override
    protected String showResults() {
        return StringUtils.join(_answers.stream().map(s -> s + "\n===END===").collect(Collectors.toList()), "\n\n");
    }
}
