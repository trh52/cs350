package org.thighfill.proctor.question;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.thighfill.proctor.JsonSaveable;
import org.thighfill.proctor.UserInterface;
import org.thighfill.proctor.question.tabulation.Tabulation;
import org.thighfill.proctor.util.ThrowingFunction;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Tobias Highfill
 */
public class Questionnaire implements JsonSaveable {

    public static List<Tabulation> tabulate(List<Questionnaire> questionnaires){
        if(questionnaires.isEmpty()){
            throw new IllegalArgumentException("Cannot tabulate 0 questionnaires");
        }
        int qCount = questionnaires.get(0).getQuestions().size();
        List<Tabulation> result = new ArrayList<>(qCount);
        for(int i=0; i < qCount; i++){
            Tabulation tab = null;
            for(Questionnaire questionnaire : questionnaires){
                Question q = questionnaire.getQuestions().get(i);
                if(tab == null){
                    tab = q.createTabulation();
                }else{
                    tab.add(q);
                }
            }
            if(tab != null){
                result.add(tab);
            }
        }
        return result;
    }

    private static final String QUESTIONS = "questions";
    private static final String TITLE = "title";
    private static final String AUTHOR = "author";
    private static final String GRADED = "graded";
    private static final String USERNAME = "username";
    private static final String DATE_TAKEN = "dateTaken";

    private static final ThreadLocal<DateFormat> DATE_FORMAT =
            ThreadLocal.withInitial(() -> new SimpleDateFormat("yyyy-MM-dd"));

    private final List<Question> _questions;
    private String _title, _author, _username;
    private boolean _graded;
    private Date _dateTaken;

    private final UserInterface _userInterface;

    public Questionnaire(UserInterface userInterface) {
        _userInterface = userInterface;
        _questions = new ArrayList<>();
    }

    public Questionnaire(UserInterface userInterface, JSONObject json) throws JSONException {
        _userInterface = userInterface;
        _title = json.has(TITLE) ? json.getString(TITLE) : null;
        _author = json.has(AUTHOR) ? json.getString(AUTHOR) : null;
        _username = json.has(USERNAME) ? json.getString(USERNAME) : null;
        try {
            _dateTaken = json.has(DATE_TAKEN) ? DATE_FORMAT.get().parse(json.getString(DATE_TAKEN)) : null;
        }
        catch (ParseException e) {
            throw new RuntimeException(e);
        }
        _graded = json.has(GRADED) && json.getBoolean(GRADED);
        if (json.has(QUESTIONS)) {
            JSONArray objects = json.getJSONArray(QUESTIONS);
            _questions = new ArrayList<>(objects.length());
            for (int i = 0; i < objects.length(); i++) {
                _questions.add(Question.fromJsonObject(userInterface, objects.getJSONObject(i)));
            }
        }
        else {
            _questions = new ArrayList<>();
        }
    }

    public void modifyWith(UserInterface ui, boolean graded) throws IOException {
        if(ui.getBoolean("Title is \""+getTitle()+"\", would you like to update it?")){
            setTitle(ui.getLine("Enter new title"));
        }
        if(ui.getBoolean("Author is \""+getAuthor()+"\", would you like to update it?")){
            setAuthor(ui.getLine("Enter new author"));
        }
    }

    public void serve() throws IOException {
        _username = getUserInterface().getLine("What is your name?");
        _dateTaken = new Date();
        for (Question question : _questions) {
            question.ask();
        }
    }

    public void grade() {
        if(!isGraded()) return;
        int gradeablePts = 0, ungradeablePts = 0, points = 0;
        for (Question question : getQuestions()) {
            int pts = question.getPoints();
            QuestionGrade questionGrade = question.grade();
            switch (questionGrade) {
            case CORRECT:
                gradeablePts += pts;
                points += pts;
                break;
            case UNANSWERED:
            case INCORRECT:
                // TODO: maybe add "SAT rules" option to deduct points for wrong (rather than omitted) answers
                gradeablePts += pts;
                break;
            case UNGRADEABLE:
                ungradeablePts += pts;
                break;
            default:
                throw new RuntimeException("Unknown grade type: " + questionGrade);
            }
        }
        getUserInterface().showMessage(
                "The user scored " + points + '/' + gradeablePts + " on gradable questions with " + ungradeablePts
                        + " points left to be graded");
    }

    @Override
    public JSONObject toJSONObject() throws JSONException {
        JSONObject res = new JSONObject()
                .put(GRADED, _graded)
                .put(TITLE, _title)
                .put(AUTHOR, _author)
                .put(USERNAME, _username)
                .put(QUESTIONS, _questions.stream().map((ThrowingFunction<Question, JSONObject>) Question::toJSONObject)
                        .collect(Collectors.toList()));
        if (_dateTaken != null) {
            res.put(DATE_TAKEN, DATE_FORMAT.get().format(_dateTaken));
        }
        return res;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(getTitle()).append("\nAuthor: ").append(getAuthor()).append('\n');
        int i = 1;
        for (Question q : _questions) {
            builder.append(i).append(") ").append(q.toString()).append('\n');
            ++i;
        }
        return builder.toString();
    }

    public void addQuestion(Question question) {
        _questions.add(question);
    }

    public UserInterface getUserInterface() {
        return _userInterface;
    }

    public List<Question> getQuestions() {
        return _questions;
    }

    public String getTitle() {
        return _title;
    }

    public void setTitle(String _title) {
        this._title = _title;
    }

    public String getAuthor() {
        return _author;
    }

    public void setAuthor(String _author) {
        this._author = _author;
    }

    public boolean isGraded() {
        return _graded;
    }

    public void setGraded(boolean _graded) {
        this._graded = _graded;
    }
}
