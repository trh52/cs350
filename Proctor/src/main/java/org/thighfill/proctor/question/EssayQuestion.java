package org.thighfill.proctor.question;

import org.json.JSONException;
import org.json.JSONObject;
import org.thighfill.proctor.UserInterface;
import org.thighfill.proctor.question.tabulation.AnswerList;
import org.thighfill.proctor.question.tabulation.Tabulation;

import java.io.IOException;

/**
 * @author Tobias Highfill
 */
public class EssayQuestion extends Question {

    public EssayQuestion(UserInterface userInterface, QuestionType type, boolean graded) throws IOException {
        super(userInterface, type, graded);
    }

    public EssayQuestion(UserInterface userInterface, JSONObject json) throws JSONException {
        super(userInterface, json);
    }

    @Override
    public void ask() throws IOException {
        setUserAnswer(getUserInterface().getMultiLine(getText()));
        setAnswered(true);
    }

    @Override
    protected void buildWith(UserInterface userInterface, boolean graded) throws IOException {
        // Do nothing
    }

    @Override
    public Tabulation createTabulation() {
        return new AnswerList(this);
    }
}
