package org.thighfill.proctor.question;

import org.json.JSONException;
import org.json.JSONObject;
import org.thighfill.proctor.JsonSaveable;
import org.thighfill.proctor.UserInterface;
import org.thighfill.proctor.question.tabulation.Tabulation;

import java.io.IOException;
import java.util.List;

/**
 * @author Tobias Highfill
 */
public abstract class Question implements JsonSaveable {

    public static Question fromJsonObject(UserInterface userInterface, JSONObject object) throws JSONException {
        String typeStr = object.getString(TYPE);
        QuestionType type = QuestionType.fromJsonID(typeStr);
        return type.getJsonConstructor().construct(userInterface, object);
    }

    private static final String QUESTION = "question";
    private static final String POINTS = "text";
    private static final String TYPE = "type";
    private static final String USER_ANSWER = "userAnswer";
    private static final String ANSWERED = "answered";

    private int _points = 1;
    private String _userAnswer = null, _text;
    private final QuestionType _type;
    private boolean _answered;
    private final UserInterface _userInterface;

    public Question(UserInterface userInterface, QuestionType type, boolean graded) throws IOException {
        _userInterface = userInterface;
        _type = type;
        _answered = false;
        setText(userInterface);
        setPoints(userInterface, graded);
    }

    public Question(UserInterface userInterface, JSONObject json) throws JSONException {
        _userInterface = userInterface;
        _text = json.getString(QUESTION);
        _answered = json.getBoolean(ANSWERED);
        _type = QuestionType.fromJsonID(json.getString(TYPE));
        if (json.has(USER_ANSWER)) {
            _userAnswer = json.getString(USER_ANSWER);
        }
        if (json.has(POINTS)) {
            _points = json.getInt(POINTS);
        }
    }

    private void setText(UserInterface userInterface) throws IOException {
        _text = userInterface.getLine("Enter the text of this question");
    }

    private void setPoints(UserInterface userInterface, boolean graded) throws IOException {
        if (graded && userInterface.getBoolean("Current points value is " + _points + ", do you wish to change it?")) {
            _points = userInterface.getInt("Number of points");
        }
    }

    public QuestionGrade grade() {
        return (!isAnswered()) ? QuestionGrade.UNANSWERED : QuestionGrade.UNGRADEABLE;
    }

    public abstract void ask() throws IOException;

    protected abstract void buildWith(UserInterface userInterface, boolean graded) throws IOException;

    public void modifyWith(UserInterface userInterface, boolean graded) throws IOException {
        if(userInterface.getBoolean("Current text is \""+getText()+"\", do you wish to change it?")){
            setText(userInterface);
        }
        setPoints(userInterface, graded);
    }

    public abstract Tabulation createTabulation();

    @Override
    public JSONObject toJSONObject() throws JSONException {
        return new JSONObject().put(QUESTION, _text).put(POINTS, _points).put(ANSWERED, _answered)
                .put(TYPE, _type.getJsonID()).put(USER_ANSWER, _userAnswer);
    }

    @Override
    public String toString() {
        return getText() + '[' + getPoints() + " pt" + (getPoints() == 1 ? "" : "s") + ']';
    }

    public String getUserAnswer() {
        return _userAnswer;
    }

    public void setUserAnswer(String answer) {
        _userAnswer = answer;
    }

    public String getText() {
        return _text;
    }

    public void setText(String text) {
        _text = text;
    }

    public int getPoints() {
        return _points;
    }

    public void setPoints(int points) {
        _points = points;
    }

    public boolean isAnswered() {
        return _answered;
    }

    public void setAnswered(boolean answered) {
        _answered = answered;
    }

    public UserInterface getUserInterface() {
        return _userInterface;
    }

    protected static void modifyChoiceList(List<String> choices, UserInterface ui) throws IOException {
        while (!choices.isEmpty() && ui.getBoolean("Would you like to remove a choice?")) {
            String rem = ui.choose("Which choice would you like to remove?", choices, true);
            if (rem != null) {
                choices.remove(rem);
            }
        }
        while (!choices.isEmpty() && ui.getBoolean("Would you like to modify a choice?")) {
            int choice = ui.chooseIndex("Which choice would you like to modify?", choices, true);
            if (choice >= 0) {
                choices.set(choice, ui.getLine("Enter the new choice"));
            }
        }
    }
}
