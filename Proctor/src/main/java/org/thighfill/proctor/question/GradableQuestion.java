package org.thighfill.proctor.question;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.thighfill.proctor.UserInterface;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * @author Tobias Highfill
 */
public abstract class GradableQuestion extends Question {

    private static final String CORRECT_ANSWERS = "correctAnswers";
    private static final String IGNORE_CASE = "ignoreCase";

    private final Set<String> _correctAnswerSet = new TreeSet<>();
    private boolean _ignoreCase;

    public GradableQuestion(UserInterface userInterface, QuestionType type, boolean graded) throws IOException {
        super(userInterface, type, graded);
    }

    public GradableQuestion(UserInterface userInterface, JSONObject json) throws JSONException {
        super(userInterface, json);
        _ignoreCase = json.has(IGNORE_CASE) && json.getBoolean(IGNORE_CASE);
        if (json.has(CORRECT_ANSWERS)) {
            JSONArray answers = json.getJSONArray(CORRECT_ANSWERS);
            for (int i = 0; i < answers.length(); i++) {
                _correctAnswerSet.add(answers.getString(i));
            }
        }
    }

    @Override
    public QuestionGrade grade() {
        return (!isAnswered()) ? QuestionGrade.UNANSWERED : checkAnswer();
    }

    private QuestionGrade checkAnswer() {
        if (_correctAnswerSet == null) {
            return QuestionGrade.UNGRADEABLE;
        }
        String user = getUserAnswer();
        boolean correct = _correctAnswerSet.contains(user);
        if (!correct && isIgnoreCase()) {
            correct = _correctAnswerSet.stream().anyMatch(ans -> ans.equalsIgnoreCase(user));
        }
        return correct ? QuestionGrade.CORRECT : QuestionGrade.INCORRECT;
    }

    @Override
    public JSONObject toJSONObject() throws JSONException {
        return super.toJSONObject().put(CORRECT_ANSWERS, _correctAnswerSet).put(IGNORE_CASE, _ignoreCase);
    }

    @Override
    public void modifyWith(UserInterface userInterface, boolean graded) throws IOException {
        super.modifyWith(userInterface, graded);
        while (graded && !_correctAnswerSet.isEmpty() && userInterface
                .getBoolean("Do you wish to remove any answers?")) {
            _correctAnswerSet.remove(userInterface
                    .choose("Which answer do you want to remove?", new ArrayList<>(_correctAnswerSet)));
        }
    }

    @Override
    public final String toString() {
        String answerStr = "";
        if (_correctAnswerSet != null) {
            answerStr = "The correct answers are [" + StringUtils
                    .join(_correctAnswerSet.stream().map(s -> '"' + StringEscapeUtils.escapeJava(s) + '"')
                            .collect(Collectors.toList()), ',') + ']';
        }
        return super.toString() + '\n' + toStringInner() + '\n' + answerStr;
    }

    protected abstract String toStringInner();

    public boolean isIgnoreCase() {
        return _ignoreCase;
    }

    public void setIgnoreCase(boolean ignoreCase) {
        _ignoreCase = ignoreCase;
    }

    public Set<String> getCorrectAnswerSet() {
        return _correctAnswerSet;
    }
}
