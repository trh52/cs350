package org.thighfill.proctor.question;

import org.thighfill.proctor.UserInterface;

import java.io.IOException;

/**
 * @author Tobias Highfill
 */
public enum QuestionType {
    MULTIPLE_CHOICE("multipleChoice", "Multiple Choice", MultipleChoiceQuestion::new, MultipleChoiceQuestion::new),
    TRUE_FALSE("trueFalse", "True/False", TrueFalseQuestion::new, TrueFalseQuestion::new),
    SHORT_ANSWER("ShortAnswer", "Short Answer", ShortAnswerQuestion::new, ShortAnswerQuestion::new),
    ESSAY("essay", "Essay", EssayQuestion::new, EssayQuestion::new),
    RANKING("ranking", "Ranking", RankingQuestion::new, RankingQuestion::new),
    MATCHING("matching", "Matching", MatchingQuestion::new, MatchingQuestion::new);

    private final String _jsonID, _friendlyName;
    private final JSONConstructor<? extends Question> _jsonConstructor;
    private final BasicConstructor<? extends Question> _basicConstructor;

    QuestionType(String jsonID, String friendlyName, JSONConstructor<? extends Question> jsonConstructor,
            BasicConstructor<? extends Question> basicConstructor) {
        _jsonID = jsonID;
        _friendlyName = friendlyName;
        _jsonConstructor = jsonConstructor;
        _basicConstructor = basicConstructor;
    }

    public Question buildFromUserInput(UserInterface ui, boolean graded) throws IOException {
        Question res = getBasicConstructor().construct(ui, this, graded);
        res.buildWith(ui, graded);
        return res;
    }

    public String getJsonID() {
        return _jsonID;
    }

    public String getFriendlyName() {
        return _friendlyName;
    }

    public JSONConstructor<? extends Question> getJsonConstructor() {
        return _jsonConstructor;
    }

    public BasicConstructor<? extends Question> getBasicConstructor() {
        return _basicConstructor;
    }

    public static QuestionType fromJsonID(String jsonID) {
        for (QuestionType type : QuestionType.values()) {
            if (type.getJsonID().equals(jsonID)) {
                return type;
            }
        }
        throw new RuntimeException("Unknown question type: " + jsonID);
    }
}
