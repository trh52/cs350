package org.thighfill.proctor;

import com.sun.speech.freetts.Voice;
import com.sun.speech.freetts.VoiceManager;

/**
 * @author Tobias Highfill
 */
public class Proctor {

    public static final String FILE_EXTENSION = ".proctor";

    public static void main(String[] args) {
        System.setProperty("freetts.voices", "com.sun.speech.freetts.en.us.cmu_us_kal.KevinVoiceDirectory");
        VoiceManager manager = VoiceManager.getInstance();
        Voice voice = manager.getVoice("kevin16");
        voice.allocate();
        System.out.println("Before speak()");
        voice.speak("Hello, world!");
        System.out.println("After speak()");
        voice.deallocate();

//        PrintStream out = new PrintStream(new OutputSpeaker());
//        Menu topMenu = new Menu(new IORedirector(out, System.in));
//        topMenu.addEntry("Survey", new SurveyMenu(topMenu));
//        topMenu.addEntry("Test", new TestMenu(topMenu));
//        topMenu.run();
    }
}