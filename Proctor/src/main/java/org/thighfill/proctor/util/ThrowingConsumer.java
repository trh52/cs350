package org.thighfill.proctor.util;

import java.util.function.Consumer;

/**
 * @author Tobias Highfill
 */
public interface ThrowingConsumer<E> extends Consumer<E> {
    @Override
    default void accept(E tmp){
        try {
            acceptThrowing(tmp);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    void acceptThrowing(E e) throws Exception;
}
