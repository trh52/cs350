package org.thighfill.proctor.util;

/**
 * @author Tobias Highfill
 */
@FunctionalInterface
public interface ThrowingRunnable extends Runnable {
    @Override
    default void run() {
        try {
            runThrow();
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    void runThrow() throws Exception;
}
