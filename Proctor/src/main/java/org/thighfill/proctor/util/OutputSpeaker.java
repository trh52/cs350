package org.thighfill.proctor.util;

import com.sun.speech.freetts.Voice;
import com.sun.speech.freetts.VoiceManager;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * @author Tobias Highfill
 */
public class OutputSpeaker extends ByteArrayOutputStream {

    private VoiceManager _voiceManager = VoiceManager.getInstance();
    private Voice _voice;

    public OutputSpeaker(String voicename){
        _voice = _voiceManager.getVoice(voicename);
        _voice.allocate();
        System.out.println("Fetched voice "+voicename);
    }

    public OutputSpeaker(){
        this("kevin16");
    }

    @Override
    public void flush() throws IOException {
        super.flush();
        String text = toString();
        System.out.println("Speaking: " + text);
        _voice.speak(text);
        reset();
    }

    @Override
    public void close() throws IOException {
        super.close();
        _voice.deallocate();
    }
}
