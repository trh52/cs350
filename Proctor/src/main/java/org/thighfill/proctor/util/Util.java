package org.thighfill.proctor.util;

import org.apache.commons.lang.StringUtils;

import java.io.*;

/**
 * @author Tobias Highfill
 */
public class Util {
    private Util() {
    }

    public static String readLineCheckNull(BufferedReader br) throws IOException {
        String res = br.readLine();
        if (res == null) {
            throw new IOException("Reached end of input!");
        }
        return res;
    }

    public static String readMultiLine(BufferedReader br) throws IOException {
        String line = readLineCheckNull(br);
        StringBuilder result = new StringBuilder();
        boolean lastBlank = false;
        while (!(lastBlank && StringUtils.isBlank(line))) {
            result.append(line).append('\n');
            lastBlank = StringUtils.isBlank(line);
            line = readLineCheckNull(br);
        }
        return result.toString();
    }

    public static String readAll(BufferedReader br) throws IOException {
        StringBuilder res = new StringBuilder();
        for (String line = br.readLine(); line != null; line = br.readLine()) {
            res.append(line).append('\n');
        }
        return res.toString();
    }

    public static String readAll(File file) throws IOException {
        try (FileReader reader = new FileReader(file); BufferedReader br = new BufferedReader(reader)) {
            return readAll(br);
        }
    }
}
