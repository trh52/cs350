package org.thighfill.proctor.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;

/**
 * This class is basically a quick and dirty way to force all my UI through a specific pipeline that I set at the start.
 * The neat bit is that I could use any {@link PrintStream} and {@link InputStream} (files, sockets, etc.).
 *
 * @author Tobias Highfill
 */
public class IORedirector {

    private final PrintStream _out;
    private final InputStream _in;

    private BufferedReader _br = null;
    private final IORedirector _copy;

    public IORedirector(PrintStream out, InputStream in) {
        _out = out;
        _in = in;
        _copy = null;
    }

    public IORedirector(IORedirector copy) {
        _out = copy._out;
        _in = copy._in;
        _br = copy._br;
        _copy = copy;
    }

    public IORedirector() {
        this(System.out, System.in);
    }

    public PrintStream getOut() {
        return _out;
    }

    public InputStream getIn() {
        return _in;
    }

    /**
     * All this is to ensure that we don't accidentally close the InputStream prematurely.
     * Basically we make all the UI copies use one {@link BufferedReader} instance.
     *
     * @return The BufferedReader for the {@link InputStream}
     */
    public BufferedReader getBufferedReader() {
        if (_br == null) {
            if (_copy != null) {
                _br = _copy.getBufferedReader();
            }
            else {
                _br = new BufferedReader(new InputStreamReader(getIn()));
            }
        }
        return _br;
    }
}
