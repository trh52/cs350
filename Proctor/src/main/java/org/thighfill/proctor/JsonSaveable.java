package org.thighfill.proctor;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Tobias Highfill
 */
public interface JsonSaveable {
    JSONObject toJSONObject() throws JSONException;
}
