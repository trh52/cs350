package org.thighfill.proctor.menu;

import org.thighfill.proctor.UserInterface;
import org.thighfill.proctor.question.Question;
import org.thighfill.proctor.question.Questionnaire;
import org.thighfill.proctor.util.IORedirector;
import org.thighfill.proctor.util.ThrowingRunnable;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Tobias Highfill
 */
public class ModificationMenu extends Menu {

    private final Questionnaire _questionnaire;
    private final boolean _graded;
    private final UserInterface _ui = new MenuInterface(this);

    public ModificationMenu(IORedirector copy, Questionnaire questionnaire, boolean graded) {
        super(copy);
        _questionnaire = questionnaire;
        _graded = graded;
        addEntry("Display", () -> getOut().println(questionnaire.toString()));
        addEntry("Add a question", new QuestionCreationMenu(this, graded, questionnaire::addQuestion));
        addEntry("Remove a question", this::remove);
        addEntry("Modify a question", (ThrowingRunnable) this::modify);
        addEntry("Modify metadata", (ThrowingRunnable) this::modifyMeta);
        addEntry("Go back", () -> setLoop(false));
    }

    private void modifyMeta() throws IOException {
        _questionnaire.modifyWith(_ui, _graded);
    }

    private Question selectQuestion() {
        List<Question> qs = _questionnaire.getQuestions();
        return qs.get(_ui.chooseIndex("Choose a question",
                qs.stream().map(Question::getText).collect(Collectors.toList())));
    }

    private void modify() throws IOException {
        selectQuestion().modifyWith(_ui, _graded);
    }

    private void remove() {
        _questionnaire.getQuestions().remove(selectQuestion());
    }
}
