package org.thighfill.proctor.menu;

import org.json.JSONException;
import org.json.JSONObject;
import org.thighfill.proctor.Proctor;
import org.thighfill.proctor.UserInterface;
import org.thighfill.proctor.question.Questionnaire;
import org.thighfill.proctor.util.IORedirector;
import org.thighfill.proctor.util.ThrowingRunnable;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Tobias Highfill
 */
public class QuestionnaireMenu extends Menu {

    final private boolean _graded;
    final private String _typeName;

    private Questionnaire _current = null;

    public QuestionnaireMenu(IORedirector copy, boolean graded, String typeName) {
        super(copy);
        _graded = graded;
        _typeName = typeName;
        addEntry("Create a new " + typeName, (ThrowingRunnable) this::createNew);
        addEntry("Display a " + typeName, this::display);
        addEntry("Load a " + typeName, this::load);
        addEntry("Save a " + typeName, (ThrowingRunnable) this::save);
        addEntry("Modify an existing " + typeName, this::modify);
        addEntry("Take a " + typeName, (ThrowingRunnable) this::take);
        addEntry("Tabulate a " + typeName, (ThrowingRunnable) this::tabulate);
        if (graded) {
            addEntry("Grade a " + typeName, this::grade);
        }
        addEntry("Go Back", this::goBack);
        addEntry("Quit", this::quit);
    }

    protected void grade() {
        if (_current == null) {
            missingError("grade");
        }
        else {
            _current.grade();
        }
    }

    protected void tabulate() throws IOException {
        List<Questionnaire> questionnaires = new ArrayList<>();
        if (_current != null) {
            questionnaires.add(_current);
        }
        UserInterface ui = new MenuInterface(this);
        while (questionnaires.isEmpty() || ui
                .getBoolean("You have " + questionnaires.size() + " questionnaire(s) loaded. Load another?")) {
            new LoaderMenu(this, questionnaires::add).run();
        }
        Questionnaire.tabulate(questionnaires).forEach(getOut()::println);
    }

    protected void take() throws IOException, JSONException {
        if (_current == null) {
            missingError("take");
        }
        else {
            _current.serve();
            save();
        }
    }

    protected void modify() {
        if (_current == null) {
            missingError("modify");
        }
        else {
            new ModificationMenu(this, _current, _graded).run();
        }
    }

    protected void goBack() {
        setLoop(false);
    }

    protected void quit() {
        System.exit(0);
    }

    protected void createNew() throws IOException {
        new CreationMenu(this, _graded, questionnaire -> _current = questionnaire).run();
    }

    protected void display() {
        if (_current == null) {
            missingError("display");
        }
        else {
            getOut().println(_current.toString());
        }
    }

    protected void load() {
        new LoaderMenu(this, (Questionnaire q) -> _current = q).run();
    }

    protected void save() throws JSONException, IOException {
        if (_current == null) {
            missingError("save");
        }
        else {
            UserInterface ui = new MenuInterface(this);
            String filename = _current.getTitle() + Proctor.FILE_EXTENSION;
            JSONObject object = _current.toJSONObject();
            do {
                if (ui.getBoolean("Will be saved as \"" + filename + "\", do you wish to change that?")) {
                    filename = ui.getLine("Enter new filename");
                }
                try (FileWriter writer = new FileWriter(filename)) {
                    object.write(writer);
                    break;
                }
                catch (IOException e) {
                    ui.showError("An error occurred while writing: "+e.getMessage());
                }
            }while(ui.getBoolean("Do you want to try again?"));
        }
    }

    private void missingError(String verb) {
        getOut().println("There is no open questionnaire to " + verb);
    }

    public boolean isGraded() {
        return _graded;
    }

    public String getTypeName() {
        return _typeName;
    }
}
