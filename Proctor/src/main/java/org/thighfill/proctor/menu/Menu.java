package org.thighfill.proctor.menu;

import org.thighfill.proctor.util.IORedirector;
import org.thighfill.proctor.util.Util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Basic class representing a command line menu
 *
 * @author Tobias Highfill
 */
public class Menu extends IORedirector implements Runnable {

    private final List<Entry> _entryList = new ArrayList<>();
    private boolean _loop = true, _repeat = false;

    public Menu(IORedirector copy) {
        super(copy);
    }

    public Menu(PrintStream out, InputStream in) {
        super(out, in);
    }

    public void addEntry(String title, Runnable target) {
        _entryList.add(new Entry(title, target));
    }

    public List<String> getTitles() {
        return _entryList.stream().map(ent -> ent._title).collect(Collectors.toList());
    }

    public int choices() {
        return _entryList.size();
    }

    @Override
    public void run() {
        boolean init = true;
        while (init || _loop || _repeat) {
            _repeat = false;
            init = false;
            int selection;
            MenuInterface ui = new MenuInterface(this);
            try {
                while (true) {
                    for (int i = 0; i < _entryList.size(); i++) {
                        getOut().println(String.format("%d) %s", i + 1, _entryList.get(i)._title));
                    }
                    selection = ui.getInt("Enter your selection");
                    if (selection < 1) {
                        getOut().println("Input cannot be less than 1, try again.");
                    }
                    else if (selection > _entryList.size()) {
                        getOut().println(
                                String.format("Input cannot be greater than %d, try again.", _entryList.size()));
                    }
                    else {
                        break;
                    }
                }
            }
            catch (IOException e) {
                throw new RuntimeException(e);
            }
            _entryList.get(selection - 1)._target.run();
        }
    }

    private int readInt(BufferedReader br) throws IOException {
        while (true) {
            String line = Util.readLineCheckNull(br);
            try {
                return Integer.parseInt(line);
            }
            catch (NumberFormatException ignore) {
                getOut().println("The input: \"" + line + "\" could not be parsed as an int.");
            }
        }
    }

    public boolean isLoop() {
        return _loop;
    }

    public void setLoop(boolean loop) {
        _loop = loop;
    }

    public boolean isRepeat() {
        return _repeat;
    }

    public void setRepeat(boolean repeat) {
        _repeat = repeat;
    }

    private static class Entry {
        final String _title;
        final Runnable _target;

        Entry(String title, Runnable target) {
            _title = title;
            _target = target;
        }
    }
}
