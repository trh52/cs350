package org.thighfill.proctor.menu;

import org.thighfill.proctor.util.IORedirector;

/**
 * @author Tobias Highfill
 */
public class TestMenu extends QuestionnaireMenu {
    public TestMenu(IORedirector copy) {
        super(copy, true, "Test");
    }
}
