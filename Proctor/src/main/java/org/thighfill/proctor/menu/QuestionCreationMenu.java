package org.thighfill.proctor.menu;

import org.thighfill.proctor.UserInterface;
import org.thighfill.proctor.question.Question;
import org.thighfill.proctor.question.QuestionType;
import org.thighfill.proctor.util.IORedirector;
import org.thighfill.proctor.util.ThrowingRunnable;

import java.util.function.Consumer;

/**
 * @author Tobias Highfill
 */
public class QuestionCreationMenu extends Menu {

    Consumer<Question> _consumer;

    public QuestionCreationMenu(IORedirector copy, boolean graded, Consumer<Question> consumer) {
        super(copy);
        _consumer = consumer;
        UserInterface ui = new MenuInterface(this);
        for (QuestionType type : QuestionType.values()) {
            addEntry("Add a new " + type.getFriendlyName() + " question",
                    (ThrowingRunnable) () -> getConsumer().accept(type.buildFromUserInput(ui, graded)));
        }
        addEntry("Go Back", this::goBack);
    }

    protected void goBack(){
        setLoop(false);
    }

    public Consumer<Question> getConsumer() {
        return _consumer;
    }

    public void setConsumer(Consumer<Question> consumer) {
        _consumer = consumer;
    }
}
