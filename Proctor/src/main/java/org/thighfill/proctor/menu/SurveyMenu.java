package org.thighfill.proctor.menu;

import org.thighfill.proctor.util.IORedirector;

/**
 * @author Tobias Highfill
 */
public class SurveyMenu extends QuestionnaireMenu {
    public SurveyMenu(IORedirector copy) {
        super(copy, false, "Survey");
    }
}
