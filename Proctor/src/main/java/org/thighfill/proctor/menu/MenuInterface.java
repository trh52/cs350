package org.thighfill.proctor.menu;

import org.thighfill.proctor.UserInterface;
import org.thighfill.proctor.util.IORedirector;
import org.thighfill.proctor.util.Util;

import java.io.IOException;
import java.util.List;

/**
 * @author Tobias Highfill
 */
public class MenuInterface extends IORedirector implements UserInterface {

    public MenuInterface(IORedirector copy) {
        super(copy);
    }

    @Override
    public String getLine(String prompt) throws IOException {
        getOut().println(prompt);
        return Util.readLineCheckNull(getBufferedReader());
    }

    @Override
    public String getMultiLine(String prompt) throws IOException {
        getOut().println(prompt + " [Enter two blank lines to finish]");
        return Util.readMultiLine(getBufferedReader());
    }

    @Override
    public <E> int chooseIndex(String prompt, List<E> choices, boolean allowCancel) {
        int[] res = {-1};
        getOut().println(prompt);
        Menu menu = new Menu(this);
        menu.setLoop(false);
        choices.forEach(ent -> menu.addEntry(ent.toString(), () -> res[0]=choices.indexOf(ent)));
        if(allowCancel){
            menu.addEntry("[Cancel]", () -> res[0]=-1);
        }
        menu.run();
        return res[0];
    }

    @Override
    public boolean getBoolean(String prompt) throws IOException {
        while (true) {
            String line = getLine(prompt + " [Y/N]");
            switch (line.toLowerCase()) {
            case "true":
            case "yes":
            case "y":
                return true;
            case "false":
            case "no":
            case "n":
                return false;
            default:
                getOut().println("Unable to parse: " + line);
                break;
            }
        }
    }

    @Override
    public long getLong(String prompt) throws IOException {
        while (true) {
            String line = getLine(prompt);
            try {
                return Long.parseLong(line);
            }
            catch (NumberFormatException ignore) {
                getOut().println("Could not parse " + line + " as a long");
            }
        }
    }

    @Override
    public void showError(String error) {
        showMessage(error);
    }

    @Override
    public void showMessage(String message) {
        getOut().println(message);
    }
}
