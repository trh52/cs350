package org.thighfill.proctor.menu;

import org.json.JSONException;
import org.json.JSONObject;
import org.thighfill.proctor.Proctor;
import org.thighfill.proctor.UserInterface;
import org.thighfill.proctor.question.Questionnaire;
import org.thighfill.proctor.util.IORedirector;
import org.thighfill.proctor.util.ThrowingRunnable;
import org.thighfill.proctor.util.Util;

import java.io.File;
import java.io.IOException;
import java.util.function.Consumer;

/**
 * This menu is for loading questionnaires
 *
 * @author Tobias Highfill
 */
public class LoaderMenu extends Menu {

    /**
     * Instantiate a loader menu. Looks for files with the correct extension in the current directory.
     * Also allows the user to input a filename by hand by selecting the last option.
     *
     * @param copy The redirector to use for I/O
     * @param consumer This consumer will receive the loaded questionnaire
     */
    public LoaderMenu(IORedirector copy, Consumer<Questionnaire> consumer) {
        super(copy);
        setLoop(false);
        File currDir = new File(".");
        UserInterface ui = new MenuInterface(this);
        File[] files = currDir.listFiles();
        if (files == null) {
            throw new RuntimeException("File list is null! Is " + currDir.getAbsolutePath() + " a regular file?");
        }
        for (File subFile : files) {
            if (subFile.getName().endsWith(Proctor.FILE_EXTENSION)) {
                addEntry(subFile.getName(), (ThrowingRunnable) () -> consumer.accept(loadFile(ui, subFile)));
            }
        }
        addEntry("Open...",
                (ThrowingRunnable) () -> consumer.accept(loadFile(ui, new File(ui.getLine("Enter filename")))));
    }

    private static Questionnaire loadFile(UserInterface ui, File f) throws IOException, JSONException {
        return new Questionnaire(ui, new JSONObject(Util.readAll(f)));
    }

    @Override
    public void run() {
        super.run();
    }
}
