package org.thighfill.proctor.menu;

import org.thighfill.proctor.question.Question;
import org.thighfill.proctor.question.Questionnaire;
import org.thighfill.proctor.util.IORedirector;

import java.io.IOException;
import java.util.function.Consumer;

/**
 * This is a menu for creating Questionnaires
 *
 * @author Tobias Highfill
 */
public class CreationMenu extends QuestionCreationMenu {

    private final Questionnaire _result;
    private final Consumer<Questionnaire> _consumer;

    /**
     * Sets up a Creation Menu
     *
     * @param copy The redirector to use for I/O
     * @param graded Indicates if the questionnaire should be graded
     * @param consumer This consumes the resulting questionnaire
     * @throws IOException
     */
    public CreationMenu(IORedirector copy, boolean graded, Consumer<Questionnaire> consumer) throws IOException {
        super(copy, graded, null);
        setConsumer(this::addQuestion);
        _consumer = consumer;
        MenuInterface ui = new MenuInterface(this);
        _result = new Questionnaire(ui);
        _result.setTitle(ui.getLine("Enter a title"));
        _result.setAuthor(ui.getLine("Enter author's name"));
        _result.setGraded(graded);
    }

    public void addQuestion(Question q){
        _result.addQuestion(q);
    }

    @Override
    protected void goBack() {
        super.goBack();
        _consumer.accept(_result);
    }
}
