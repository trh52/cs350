import org.junit.Test;

import java.util.Random;
import java.util.stream.Stream;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * @author Tobias Highfill
 */
public class MergeSortTest {

    private static final int RANDOM_COUNT = 500;

    private Random _random = new Random();

    @Test
    public void creationTest(){
        assertNotNull(new MergeSort());
    }

    @Test
    public void mainTest(){
        MergeSort.main(new String[]{});
    }

    @Test
    public void testIntegers() {
        check(new Integer[] { 5, 2, 1, 4, 3 }, new Integer[] { 1, 2, 3, 4, 5 });
        check(_random.ints().boxed(), new Integer[RANDOM_COUNT]);
    }

    @Test
    public void testDoubles(){
        check(_random.doubles().boxed(), new Double[RANDOM_COUNT]);
    }

    @Test
    public void testStrings(){
        check(new String[]{"Aardvark", "Phlegathon", "Tom Brady", "007 Nightfire", "!@#$%"});
    }

    private <T extends Comparable<T>> void check(Stream<T> stream, T[] arr){
        check(stream.limit(arr.length).toArray(i->arr));
    }

    private void check(Comparable[] unsorted, Comparable[] presorted) {
        MergeSort.mergeSort(unsorted);
        assertArrayEquals(unsorted, presorted);
    }

    private <T extends Comparable<T>> void check(T[] test){
        MergeSort.mergeSort(test);
        assertTrue("Result is not sorted!", isSorted(test));
    }

    private <T extends Comparable<T>> boolean isSorted(T[] arr) {
        if (arr.length <= 1) {
            return true;
        }
        T prev = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (prev.compareTo(arr[i]) > 0) {
                return false;
            }
            prev = arr[i];
        }
        return true;
    }
}
