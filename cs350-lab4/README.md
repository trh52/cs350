CS 350 - Lab 4
=====

##Building the test coverage report

```
    make
```

##Viewing it

Go into `build\reports\jacoco\test\html` and open `index.html`
in your favorite browser.