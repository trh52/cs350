public class main {

    public static void main(String[] args) { 
	AlarmClock myClock = new AlarmClock(8, 0, 8, 5, "1060 AM");
	for (int i = 0; i < 5; i++)
	    {
		System.out.println("Time: " + myClock.showTime());
		for (int seconds = 0; seconds < 60; seconds++)
		    {
			myClock.tick();
			myClock.checkAlarm();
		    }
	    }
	myClock.snooze();
	for (int i = 0; i < 9; i++)
	    {
		System.out.println("Time: " + myClock.showTime());
		for (int seconds = 0; seconds < 60; seconds++)
		    {
			myClock.tick();
			myClock.checkAlarm();
		    }
	    }
	myClock.alarmOff();
    }
}
