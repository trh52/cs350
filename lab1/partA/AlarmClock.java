
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class AlarmClock{
    private static final DateTimeFormatter FORMAT = DateTimeFormatter.ofPattern("hh:mm a");
    private static final int SNOOZE_TIME = 9;
    
    private LocalTime alarmTime, currTime;
    private boolean alarmOn, radioOn;
    private String radio;
    private int volume=0, snooze=0;

    public AlarmClock(int cth, int ctm, int ath, int atm, String radio){
	alarmTime = LocalTime.of(ath, atm);
	currTime = LocalTime.of(cth, ctm);
	this.radio = radio;
	radioOn = alarmOn = true;
	System.out.println(String.format("Alarm clock created. time = %s, alarm = %s, alarm on: %b, radio on: %b"
					 +", radio station = %s, volume = %d",
					 showTime(), alarmTime.format(FORMAT), alarmOn, radioOn, radio, volume));
    }

    public String showTime(){
	return formatTime(currTime);
    }

    public String showAlarmTime(){
	return formatTime(alarmTime);
    }

    public void tick(){
	setTime(currTime.plusSeconds(1));
    }

    public void snooze(){
	snooze += SNOOZE_TIME;
    }

    public void checkAlarm(){
	if(alarmOn && currTime.equals(alarmTime.plusMinutes(snooze))){
	    System.out.println("Buzz Buzz Buzz");
	    snooze = 0;
	}
    }

    public void alarmOff(){
	setAlarmOn(false);
    }

    public boolean isAlarmOn(){
	return alarmOn;
    }

    public void setAlarmOn(boolean alarmOn){
	this.alarmOn = alarmOn;
    }

    public boolean isRadioOn(){
	return radioOn;
    }

    public void setRadioOn(boolean radioOn){
	this.radioOn = radioOn;
    }

    public LocalTime getTime(){
	return currTime;
    }
    
    public void setTime(LocalTime newTime){
	currTime = newTime;
    }

    public LocalTime getAlarm(){
	return alarmTime;
    }

    public void setAlarm(LocalTime newAlarm){
	alarmTime = newAlarm;
    }

    public String getRadio(){
	return radio;
    }

    public void setRadio(String newRadio){
	radio = newRadio;
    }

    public int getVolume(){
	return volume;
    }
    
    public void setVolume(int volume){
	volume = volume;
    }

    private static String formatTime(LocalTime time){
	return time.format(FORMAT);
    }
}
